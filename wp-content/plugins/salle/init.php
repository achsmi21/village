<?php
/*
Plugin Name: salle
Description:
Version: 1
Author: village.com
Author URI: http://village.com
*/
// function to create the DB / Options / Defaults

// run the install scripts upon plugin activation
register_activation_hook(__FILE__, 'ss_options_install');

//menu items
add_action('admin_menu','village_salle_modifymenu');
function village_salle_modifymenu() {
	
	//this is the main item for the menu
	add_menu_page('salle', //page title
	'salle', //menu title
	'manage_options', //capabilities
	'village_salle_list', //menu slug
	'village_salle_list' //function
	);
	
	//this is a submenu
	add_submenu_page('village_salle_list', //parent slug
	'Add New Salle', //page title
	'Add New', //menu title
	'manage_options', //capability
	'village_salle_create', //menu slug
	'village_salle_create'); //function
	
	//this submenu is HIDDEN, however, we need to add it anyways
	add_submenu_page(null, //parent slug
	'Update Salle', //page title
	'Update', //menu title
	'manage_options', //capability
	'village_salle_update', //menu slug
	'village_salle_update'); //function
}
define('ROOTDIR', plugin_dir_path(__FILE__));
require_once(ROOTDIR . 'salle-list.php');
require_once(ROOTDIR . 'salle-create.php');
require_once(ROOTDIR . 'salle-update.php');
