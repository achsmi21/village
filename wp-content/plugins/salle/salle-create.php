<?php

function village_salle_create()
{
    global $wpdb;

    //insert
    if (isset($_POST['insert'])) {
        global $wpdb;
        $table_name = $wpdb->prefix . "salle";

        $data = [
            'name' => $_POST["name"],
            'capacity' => $_POST["capacity"],
            'description' => $_POST["description"],
            'superficie' => $_POST["superficie"],
            'tarif' => $_POST["tarif"],
            'is_sanitary' => true,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),
            'status' => 1
        ];
        // var_dump($data) ;
        $wpdb->insert(
            $table_name, //table
            $data //data
        );
        // die();
        $message .= "Salle inserted";
    }
    ?>
    <link type="text/css" href="<?php echo WP_PLUGIN_URL; ?>/bootstrap\css\bootstrap.css" rel="stylesheet"/>
    <div style="margin-top:10px" class="content">
    <div class="row">
    <div class="col-lg-12">
    <div class="panel panel-default">
    <div class="panel-heading">
        Ajouter une
        <small>Salle</small>
        <a class="btn btn-success btn-xs pull-right"
           href="<?php echo admin_url('admin.php?page=village_salle_list'); ?>"><span
                    class="icon-plus"></span> liste</a>

    </div>
    <div class="panel-body">
        <?php if (isset($message)): ?>
            <div class="updated"><p><?php echo $message; ?></p></div><?php endif; ?>


        <form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
            <div class="form-group">
                <label for="name">Nom</label>
                <input type="text" required class="form-control" name="name" id="name" placeholder="Nom">
            </div>
            <div class="form-group">
                <label for="capacity">Capacité d'accueil</label>
                <input type="number" required class="form-control" name="capacity" id="capacity"
                       placeholder="Capacité d'accueil">
            </div>
            <div class="form-group">
                <label for="description">Description</label>
                <textarea class="form-control" name="description" id="description" rows="3"></textarea>
            </div>
            <div class="form-group">
                <label for="superficie">Superficie</label>
                <input type="number" required name="superficie" class="form-control" id="superficie"
                       placeholder="Superficie">
            </div>
            <div class="form-group">
                <label for="tarif">Tarif</label>
                <input type="text" name="tarif" required class="form-control" id="tarif" placeholder="Tarif">
            </div>
            <button type="submit" name="insert" class="btn btn-primary mb-2">Sauvgarder</button>

        </form>


    </div>
    <?php
}