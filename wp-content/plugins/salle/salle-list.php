<?php

function village_salle_list()
{
    ?>
    <link type="text/css" href="<?php echo WP_PLUGIN_URL; ?>/bootstrap\css\bootstrap.css" rel="stylesheet"/>
    <div style="margin-top:10px" class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Liste des |
                        <small>Salle</small>
                        <a class="btn btn-success btn-xs pull-right"
                           href="<?php echo admin_url('admin.php?page=village_salle_create'); ?>"><span
                                    class="icon-plus"></span> Ajoute</a>

                    </div>
                    <div class="panel-body">


                        <?php
                        global $wpdb;
                        $table_name = $wpdb->prefix . "salle";

                        $rows = $wpdb->get_results("SELECT * from $table_name");
                        ?>
                        <table id="datatable2" class="table table-striped table-hover">


                            <thead>

                            <tr>
                                <th class="manage-column ss-list-width">Nom</th>
                                <th class="manage-column ss-list-width">Capacité d'accueil</th>
                                <th class="manage-column ss-list-width">Superficie</th>
                                <th class="manage-column ss-list-width">Tarif</th>
                                <th>&nbsp;</th>
                            </tr>
                            </thead>
                            <tbody>

                            <?php foreach ($rows as $row) {
                                ?>
                                <tr>
                                    <td class="manage-column ss-list-width"><?php echo $row->name; ?></td>
                                    <td class="manage-column ss-list-width"><?php echo $row->capacity; ?></td>
                                    <td class="manage-column ss-list-width"><?php echo $row->superficie; ?></td>
                                    <td class="manage-column ss-list-width"><?php echo $row->tarif; ?></td>
                                    <td>
                                        <a class="btn btn-warning btn-xs"
                                           href="<?php echo admin_url('admin.php?page=village_salle_update&id=' . $row->id); ?>">Update</a>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php
}