<?php

function village_salle_update()
{
    global $wpdb;
    $table_name = $wpdb->prefix . "salle";
    $id = $_GET["id"];
//update
    if (isset($_POST['update'])) {

        $data = [
            'name' => $_POST["name"],
            'capacity' => $_POST["capacity"],
            'description' => $_POST["description"],
            'superficie' => $_POST["superficie"],
            'tarif' => $_POST["tarif"],
            'is_sanitary' => true,
            'updated_at' => date("Y-m-d H:i:s"),
        ];


        $wpdb->update(
            $table_name, //table
            $data, //data
            array('ID' => $id), //where
            array('%s'), //data format
            array('%s') //where format
        );
    } //delete
    else if (isset($_POST['delete'])) {
        $wpdb->query($wpdb->prepare("DELETE FROM $table_name WHERE id = %s", $id));
    } else {//selecting value to update	
        $salle = $wpdb->get_results($wpdb->prepare("SELECT * from $table_name where id=%s", $id));
        $salle = $salle[0];

    }
    ?>
    <link type="text/css" href="<?php echo WP_PLUGIN_URL; ?>/bootstrap\css\bootstrap.css" rel="stylesheet"/>
    <div style="margin-top:10px" class="content">
    <div class="row">
    <div class="col-lg-12">
    <div class="panel panel-default">
    <div class="panel-heading">
        Modifier une
        <small>Salle</small>
        <a class="btn btn-success btn-xs pull-right"
           href="<?php echo admin_url('admin.php?page=village_salle_list'); ?>"><span
                    class="icon-plus"></span> liste</a>

    </div>
    <div class="panel-body">

        <?php if ($_POST['delete']) { ?>
            <div class="updated"><p>Salle deleted</p></div>
            <a href="<?php echo admin_url('admin.php?page=village_salle_list') ?>">&laquo; Back to salle list</a>

        <?php } else if ($_POST['update']) { ?>
            <div class="updated"><p>Salle updated</p></div>
            <a href="<?php echo admin_url('admin.php?page=village_salle_list') ?>">&laquo; Back to salle list</a>

        <?php } else { ?>
            <form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">


                <div class="form-group">
                    <label for="name">Nom</label>
                    <input type="text" required value="<?php echo $salle->name ?>" class="form-control" name="name"
                           id="name" placeholder="Nom">
                </div>
                <div class="form-group">
                    <label for="capacity">Capacité d'accueil</label>
                    <input type="number" value="<?php echo $salle->capacity ?>" required class="form-control"
                           name="capacity" id="capacity"
                           placeholder="Capacité d'accueil">
                </div>
                <div class="form-group">
                    <label for="description">Description</label>
                    <textarea class="form-control" name="description" id="description" rows="3">
                        <?php echo $salle->description ?>
                    </textarea>
                </div>
                <div class="form-group">
                    <label for="superficie">Superficie</label>
                    <input type="number" value="<?php echo $salle->superficie ?>" required name="superficie"
                           class="form-control" id="superficie"
                           placeholder="Superficie">
                </div>
                <div class="form-group">
                    <label for="tarif">Tarif</label>
                    <input type="text" value="<?php echo $salle->tarif ?>" name="tarif" required class="form-control"
                           id="tarif" placeholder="Tarif">
                </div>

                <input type='submit' name="update" value='Save' class='button'> &nbsp;&nbsp;
                <input type='submit' name="delete" value='Delete' class='button'
                       onclick="return confirm('&iquest;Est&aacute;s seguro de borrar este elemento?')">
            </form>
        <?php } ?>

    </div>
    <?php
}