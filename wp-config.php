<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier contient les réglages de configuration suivants : réglages MySQL,
 * préfixe de table, clés secrètes, langue utilisée, et ABSPATH.
 * Vous pouvez en savoir plus à leur sujet en allant sur
 * {@link http://codex.wordpress.org/fr:Modifier_wp-config.php Modifier
 * wp-config.php}. C’est votre hébergeur qui doit vous donner vos
 * codes MySQL.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en "wp-config.php" et remplir les
 * valeurs.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define('DB_NAME', 'village');

/** Utilisateur de la base de données MySQL. */
define('DB_USER', 'root');

/** Mot de passe de la base de données MySQL. */
define('DB_PASSWORD', '');

/** Adresse de l’hébergement MySQL. */
define('DB_HOST', 'localhost');

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define('DB_CHARSET', 'utf8mb4');

/** Type de collation de la base de données.
  * N’y touchez que si vous savez ce que vous faites.
  */
define('DB_COLLATE', '');

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '^=1VJjia-#6JQy`xwek9YC=4z<5z^TQ3C.HoOtzwBNzUEM9?3eSEY(hj=zLhjhs~');
define('SECURE_AUTH_KEY',  '1`KN%#.A.jd:2<Xv]0=YNKUzv4EFVj3:/RRx1<_@lm#x//b4u#n~.M(^+Pzy(pK<');
define('LOGGED_IN_KEY',    '(6Ff_$*zPsf`codTBz>0jf;IWLZ[K-r};)`a#z=k`ks7bT$!:BVr7,YWbGo&nv7%');
define('NONCE_KEY',        't4D7c!J@S]Ecq}ul.WXp(ugX8j(KrDM<a~ZED:1*t?[P-,?}A?9GWK[th^GRka4;');
define('AUTH_SALT',        'hNqjj9vnjjZAJlAIhq37 OcXdo{@9,BP,5+;%<VI_ay~{Uy~$JCP8)4v9$6pVLOo');
define('SECURE_AUTH_SALT', 'Avq-0.Zb;g0S*em& qZb4]<je}6j|Ka@ZECk~X|:DaR+U(TtJ;Ve0?z0e.vJ4o4@');
define('LOGGED_IN_SALT',   'r/J|[uQY{5o8iwiu2aqQ^52>:`3p^%P77W:bA/3<H+L#3Q!{&:XA:%A9b}<@2E<|');
define('NONCE_SALT',       'ibg_Qwpv&*Q 5B9M9pvoz%y7Lf&.f>a~G$0E-`Y!5_;k7^W8$HGowm#`.77^r-r ');
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix  = 'wp_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortemment recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* C’est tout, ne touchez pas à ce qui suit ! */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');